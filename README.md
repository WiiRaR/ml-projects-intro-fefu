# Intorduction to python machine learning projects by examples


## Getting started

Quick steps to start:

1. Requires python>=3.6, pip, venv

```
cd <existing_repo>
git clone https://gitlab.com/zhilenkov/ml-projects-intro-fefu.git
cd ml-projects-intro-fefu
python3 -m venv venv
source venv/bin/activate
pip install --upgrade pip
pip install -r requirments.txt
pip install -e .
mkdir data logs
```

> Do not create new virtual envs on cuba.dvfu.ru. Use /home/zhil/ml-projects-intro-fefu/venv.

## Name

ml-projects-intro-fefu

## Description

This project designed as an introductory class for Far Eastern Federal University students. The central part of the project is a package 'mlintropack' into src directory.

## Testing

In order to see testing coverage report, use command:
```
pytest --cov mlintropack --cov-report html
```

## Project UML schemes

```mermaid
classDiagram
Class01 <|-- AveryLongClass : Cool
```

## Roadmap

General structure of the project is developed according to modern trends for python projects

## Contributing

This project is opened for contribution from FEFU students attending ML classes included to 'math models' course. In order to contribute to this project, please, use standard approach with forks and merge requests. You can find the details on YouTube or via the links below, in 'collaborate' section.

## Authors

D.V. Zhilenkov

## Project status

Dev.

***

# Default gitlab README topics

## Collaborate with your team


- [ ] [Invite team members and collaborators](https://docs.gitlab.com/ee/user/project/members/)
- [ ] [Create a new merge request](https://docs.gitlab.com/ee/user/project/merge_requests/creating_merge_requests.html)
- [ ] [Automatically close issues from merge requests](https://docs.gitlab.com/ee/user/project/issues/managing_issues.html#closing-issues-automatically)
- [ ] [Enable merge request approvals](https://docs.gitlab.com/ee/user/project/merge_requests/approvals/)
- [ ] [Automatically merge when pipeline succeeds](https://docs.gitlab.com/ee/user/project/merge_requests/merge_when_pipeline_succeeds.html)

## Test and Deploy

Use the built-in continuous integration in GitLab.

- [ ] [Get started with GitLab CI/CD](https://docs.gitlab.com/ee/ci/quick_start/index.html)
- [ ] [Analyze your code for known vulnerabilities with Static Application Security Testing(SAST)](https://docs.gitlab.com/ee/user/application_security/sast/)
- [ ] [Deploy to Kubernetes, Amazon EC2, or Amazon ECS using Auto Deploy](https://docs.gitlab.com/ee/topics/autodevops/requirements.html)
- [ ] [Use pull-based deployments for improved Kubernetes management](https://docs.gitlab.com/ee/user/clusters/agent/)
- [ ] [Set up protected environments](https://docs.gitlab.com/ee/ci/environments/protected_environments.html)

***

# Types of neural networks

## Feed Forward Neural Network

![FFNN](lectures_data/ffnn.png)

## Convolutional Neural Network

![CNN](lectures_data/cnn.png)

## Recurrent Neural Network

![RNN](lectures_data/rnn.jpeg)

## Message Passing Neural Network (Graph Neural Network)

![MPNN](lectures_data/mpnn.png)

## Transformer Example

![Transformer parts](lectures_data/transformer_parts.png)

![Transformer](lectures_data/transformer.png)

# RL introductory lecture

This is a short lecture materials for Reinforcement Learning concept with useful links.


## Base concept

![Interactions between agent and Environment](lectures_data/rl_base.jpg "Interactions between agent and Environment")

## Markov process

![Markov chain](lectures_data/markov_chain.png)

## Dataset

| s             | a             | r             |
|---------------|---------------|---------------|
| s<sub>0</sub> | a<sub>0</sub> | r<sub>0</sub> |
| ...           | ...           | ...           |
| s<sub>m</sub> | a<sub>m</sub> | r<sub>m</sub> |


## Pendulum Example

![Pendulum](lectures_data/pendulum.jpeg)

## Q-learning

All formulas are taken from OpenAI Spinning Up page. Link is given below.

![Action definition](lectures_data/action_definition.svg)

![State definition](lectures_data/state_definition.svg)

![Trajectory](lectures_data/trajectory.svg)

![Trajectory discounted reward](lectures_data/discounted_overall_reward.svg)

![V-function](lectures_data/on_policy_V.svg)

![Q-function](lectures_data/on_policy_Q.svg)

![QV evaluations](lectures_data/on_policy_V_Q.svg)

![Q-learning](lectures_data/q_updates.svg)

![Action choice](lectures_data/action.svg)

## Links
1. [RL Course by David Silver](https://www.youtube.com/watch?v=2pWv7GOvuf0&list=PLzuuYNsE1EZAXYR4FJ75jcJseBmo4KQ9-)
2. [OpenAI Spinning Up](https://spinningup.openai.com/en/latest/spinningup/rl_intro.html)
3. [Введение в различные алгоритмы обучения с подкреплением](https://habr.com/ru/post/561746/)
4. [Playing Atari with Deep Reinforcement Learning](https://arxiv.org/abs/1312.5602)