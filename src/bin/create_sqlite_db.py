"""
The script with cumbersome logger (internal logging library in python).
"""

import sqlite3
import logging

from sqlite3 import Error
from pathlib import Path
from config import PathConfiguration


def create_sqlite_connection(db_file: Path, logger: logging.Logger) -> None:
    """
    Create a database connection to a SQLite database. Create database if it doesn't exist.
    This code is taken from https://www.sqlitetutorial.net/sqlite-python/creating-database/

    Args:
        logger (logging.Logger): Configured logger
        db_file (str, pathlib.Path): A path to sqlite3 database file

    """
    conn = None
    try:
        conn = sqlite3.connect(str(db_file.absolute()))
        logger.debug("Connected/created SQLite3 DB.")
    except Error as e:
        logger.error(e)
    finally:
        if conn:
            conn.close()

    return


def main():

    c = PathConfiguration()  # Add configuration

    # Initialize logger (Too hard, loguru or smth like that is preferable)
    logger = logging.getLogger(__name__)
    logger.setLevel(logging.DEBUG)
    formatter = logging.Formatter("%(levelname)s - %(name)s - %(asctime)s - %(message)s")
    file_handler = logging.FileHandler(c.project / c.logs / (Path(__file__).name[:-3] + "_debug.log"))
    file_handler.setLevel(logging.DEBUG)
    file_handler.setFormatter(formatter)
    stream_handler = logging.StreamHandler()
    stream_handler.setLevel(logging.INFO)
    stream_handler.setFormatter(formatter)
    logger.addHandler(file_handler)
    logger.addHandler(stream_handler)

    # Run logic
    logger.info("Starting SQLite3 DB creation.")
    create_sqlite_connection(c.db, logger)
    logger.info("SQLite3 DB created.")

    return


if __name__ == "__main__":
    main()
