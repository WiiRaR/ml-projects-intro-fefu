from abc import ABC, abstractmethod


class Interface(ABC):

    @abstractmethod
    def do_smth(self):
        pass


class FollowerOne(Interface):

    def do_smth(self):
        print(f"Doing something in first way.")


class FollowerTwo(Interface):

    def do_smth(self):
        print("Doing something in second way.")


def dummy_function(object_following_interface: Interface):
    object_following_interface.do_smth()


if __name__ == "__main__":
    follower_one = FollowerOne()
    follower_two = FollowerTwo()
    print("Use function with first user-defined type:")
    dummy_function(follower_one)
    print("Use function with second user-defined type:")
    dummy_function(follower_two)
