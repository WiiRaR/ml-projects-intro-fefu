"""
The code is taken from kaggle https://www.kaggle.com/code/zheedong/cat-dog-pytorch.

Author: zheedong.

Instructions to start this script:
- Download cat-vs-dog dataset, available on kaggle https://www.kaggle.com/c/dogs-vs-cats
- Make 'data' directory.
- Extract dogs-vs-cats data to 'data/cats-vs-dogs' directory.
- The script is ready to start. Data will be extracted and structured there in train and test folders.
"""

import pandas as pd  # data processing, CSV file I/O (e.g. pd.read_csv)

import torch
from torch import nn
from torch.utils.data import DataLoader, random_split
import torch.nn.functional as F

import torchvision
from torchvision import transforms, models

import matplotlib.pyplot as plt

import zipfile

from pathlib import Path

base_dir = Path(__file__).parent / "data/cats-vs-dogs"
train_zip = base_dir / "train.zip"
test_zip = base_dir / "test1.zip"

validate_existence = [
    not base_dir.exists(),
    not train_zip.exists(),
    not test_zip.exists()
]

if any(validate_existence):
    raise OSError("There is no .zip archives with cat-vs-dogs data")

train_dir = Path(__file__).parent / "data/cats-vs-dogs/train"

if not train_dir.exists():
    print("Extracting")
    with zipfile.ZipFile(train_zip, "r") as z:
        z.extractall(path=Path(__file__).parent / "data/cats-vs-dogs")
    with zipfile.ZipFile(test_zip, "r") as z:
        z.extractall(path=Path(__file__).parent / "data/cats-vs-dogs")


# Make directories to save data (For ImageFolder)

(train_dir / "cat").mkdir(exist_ok=True)
(train_dir / "dog").mkdir(exist_ok=True)

# Move data according to its name
for filename in train_dir.rglob("*.jpg"):
    if "cat" in str(filename.stem):
        filename.rename(f"{train_dir}/cat/{filename.name}")
    if "dog" in str(filename.stem):
        filename.rename(f"{train_dir}/dog/{filename.name}")

data_transform = transforms.Compose([
    transforms.Resize((256, 256)),
    transforms.ToTensor(),
    # transforms.Normalize((0.5, 0.5, 0.5), (0.5, 0.5, 0.5))
])

dataset = torchvision.datasets.ImageFolder(root=str(train_dir), transform=data_transform)
idx_to_class = {v: k for k, v in dataset.class_to_idx.items()}

data_size = len(dataset)
# Random split dataset
train_dataset, validation_dataset = random_split(dataset, [int(0.8 * data_size), int(0.2 * data_size)])

train_dataloader = DataLoader(train_dataset, batch_size=1, shuffle=True)
validation_dataloader = DataLoader(validation_dataset, batch_size=1, shuffle=True)

# Uncomment this block if you want to see picture example.
# for value in train_dataloader:
#     data, label = value
#     print(data.shape)
#     print(f"This is... {idx_to_class[int(label.item())]}")
#     tmp = torch.squeeze(data).permute(1, 2, 0)
#     plt.imshow(torch.squeeze(data).permute(1, 2, 0))
#     plt.show()
#     break

device = "cpu"
print(f"Using {device} device")


# Accuracy 50% model == Equal to random choice
class VanillaMyNet(nn.Module):
    def __init__(self):
        super().__init__()

        self.conv1 = nn.Conv2d(in_channels=3, out_channels=12, kernel_size=5, stride=1, padding=1)
        self.conv2 = nn.Conv2d(in_channels=12, out_channels=24, kernel_size=3, stride=1, padding=1)
        self.bn1 = nn.BatchNorm2d(24)
        self.pool = nn.MaxPool2d(2, 2)
        self.conv3 = nn.Conv2d(in_channels=24, out_channels=12, kernel_size=5, stride=1, padding=1)
        self.bn2 = nn.BatchNorm2d(12)
        self.fc1 = nn.Linear(187500, 2)

    def forward(self, input_data):
        tmp = F.relu(self.bn1(self.conv2(self.conv1(input_data))))
        tmp = self.pool(tmp)
        tmp = F.relu(self.bn2(self.conv3(tmp)))
        tmp = tmp.view(-1, 187500)
        tmp = self.fc1(tmp)
        return tmp


resnet18_pretrained = models.resnet18(pretrained=True, progress=True)
num_features = resnet18_pretrained.fc.in_features
num_classes = 2
resnet18_pretrained.fc = nn.Linear(num_features, num_classes)
print(resnet18_pretrained)


def train(dataloader, model, loss_fn, optimizer):
    size = len(dataloader.dataset)
    for batch, (x, y) in enumerate(dataloader):
        x, y = x.to(device), y.to(device)

        prediction = model(x)
        loss = loss_fn(prediction, y)

        optimizer.zero_grad()
        loss.backward()
        optimizer.step()

        if batch % 100 == 0:
            loss, current = loss.item(), batch * len(x)
            print(f"loss: {loss:>7f} [{current:>5d}/{size:>5d}]")


def validation(dataloader, model, loss_fn):
    size = len(dataloader.dataset)
    num_batches = len(dataloader)
    model.eval()
    test_loss, correct = 0, 0
    with torch.no_grad():
        for x, y in dataloader:
            x, y = x.to(device), y.to(device)
            prediction = model(x)
            test_loss += loss_fn(prediction, y).item()
            correct += (prediction.argmax(1) == y).type(torch.float).sum().item()
    test_loss /= num_batches
    correct /= size
    print(f"Test Error: \n Accuracy: {(100*correct):>0.1f}%, Avg loss: {test_loss:>8f}\n")


# model = resnet18_pretrained.to(device)
model = VanillaMyNet()

loss_fn = nn.CrossEntropyLoss()
optimizer = torch.optim.Adam(model.parameters(), lr=1e-5)

epochs = 2
for t in range(epochs):
    print(f"Epoch {t+1}\n---------------------")
    train(train_dataloader, model, loss_fn, optimizer)
    validation(validation_dataloader, model, loss_fn)
print("Done!")

test_dir = Path(__file__).parent / "data/cats-vs-dogs/test1"
# Make directories to save data (For ImageFolder)

(test_dir / "test").mkdir(exist_ok=True)

for filename in test_dir.rglob("*.jpg"):
    if filename.exists():
        filename.rename(f"{test_dir}/test/{filename.name}")

test_dataset = torchvision.datasets.ImageFolder(root=str(test_dir), transform=data_transform)
test_dataloader = DataLoader(test_dataset, batch_size=1, shuffle=False)

for value in train_dataloader:
    data, _ = value
    print(data.shape)
    plt.imshow(torch.squeeze(data).permute(1, 2, 0))
    break


def test(dataloader, model):
    size = len(dataloader.dataset)
    num_batches = len(dataloader)
    model.eval()
    predictions = []
    with torch.no_grad():
        for value in dataloader:
            x, _ = value
            x = x.to(device)
            y_predicted = model(x).cpu().data.numpy().argmax()
            predictions.append(y_predicted)
    return predictions


predictions = test(test_dataloader, model)

sample = pd.read_csv(str(base_dir / "sampleSubmission.csv"))
i = sample.id


# dog = 1, cat = 0
def get_label(prediction):
    return 1 if idx_to_class[prediction] == "dog" else 0


output = pd.DataFrame({'id': i, 'label': list(map(get_label, predictions))})
output.head(20)


