import numpy as np
import matplotlib.pyplot as plt

from sklearn.linear_model import LinearRegression
from mlintropack.datagens.zhilenkov import LinearDataGenerator


def plot_simple_linear():
    linear_datagen = LinearDataGenerator(1, 50)
    x, y = linear_datagen.get_data()
    plt.figure()
    plt.scatter(x, y)
    plt.show()
    plt.close()

    model = LinearRegression()
    model.fit(x, y)
    p = model.predict(x)
    plt.figure()
    plt.scatter(x, y)
    plt.plot(x, p, c="red")
    plt.show()
    plt.close()


def plot_tricky_linear():
    linear_datagen = LinearDataGenerator(1, 100)
    x, y, t = linear_datagen.generate_tricky_data_example()
    plt.figure()
    plt.scatter(x, y)
    plt.show()
    plt.close()

    model = LinearRegression()
    model.fit(x.reshape(-1, 1), y.reshape(-1, 1))
    p = model.predict(x.reshape(-1, 1))

    plt.figure()
    plt.scatter(x, y)
    plt.plot(x, p, c="red")
    plt.show()
    plt.close()

    fig = plt.figure()
    ax = fig.add_subplot(111, projection='3d')
    for i in range(len(x)):
        color = "blue" if t[i] else "red"
        ax.scatter(x[i], t[i], y[i], c=color)
    plt.show()
    plt.close()

    dataset = np.vstack([x, t]).T
    label = y.reshape(-1, 1)
    model.fit(dataset, label)

    num_points = 2000
    points = np.random.rand(num_points, 2)
    points *= 16.
    points[:, 0] -= np.ones(shape=(num_points, )) * 8.
    points[:, 1] -= np.ones(shape=(num_points, )) * 4.
    predictions_on_random_points = model.predict(points)
    points = np.hstack([points, predictions_on_random_points])
    fig = plt.figure()
    ax = fig.add_subplot(111, projection='3d')
    for i in range(len(x)):
        color = "blue" if t[i] else "red"
        ax.scatter(x[i], t[i], y[i], c=color)
    ax.scatter(points[:, 0], points[:, 1], points[:, 2], alpha=0.3, s=0.5, c="purple")
    plt.show()
    plt.close()

    return


def main():
    plot_tricky_linear()


if __name__ == "__main__":
    main()
