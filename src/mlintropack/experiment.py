from mlintropack.models.model import Model
from dataclasses import dataclass
from mlintropack.dataflows.dataflow import DataFlow
from mlintropack.datagens.datagen import DataGen
from mlintropack.preprocessors.preprocessor import Preprocessor
from mlintropack.splitter import Splitter

from typing import Optional, Union
from logging import Logger

from datetime import datetime


@dataclass
class Experiment:
    """
    ML-experiment class
    """

    loader: Union[DataFlow, DataGen]
    preprocess: Preprocessor
    split: Splitter
    model: Model
    saver: Optional[DataFlow]
    logger: Logger

    def conduct(self):
        """
        Method to run an experiment.

        Returns:

        """
        self.logger.info(f"Experiment starts at {datetime.now().strftime('%d-%m-%Y %H:%M:%S')}.")
        dataset = self.loader.get_data()
        processed_data = self.preprocess(dataset)
        train, test = self.split(processed_data)
        self.model.fit(train)
        result = self.model.predict(test)
        if self.saver:
            self.saver.save(result)
        return result
