import itertools


def batched(iterable, n):
    """
    Batch data into lists of length n. The last batch may be shorter.
    Official itertools recipe.
    """
    # batched('ABCDEFG', 3) --> ABC DEF G
    if n < 1:
        raise ValueError('n must be at least one')
    it = iter(iterable)
    while batch := list(itertools.islice(it, n)):
        yield batch
