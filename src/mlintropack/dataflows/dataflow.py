from abc import ABC, abstractmethod


class DataFlow(ABC):
    """
    An interface for any input/output manipulations with data.
    """

    @abstractmethod
    def get_data(self, *args, **kwargs):
        """
        Load data from the source specified in args or kwargs.
        """

    @abstractmethod
    def save(self, *args, **kwargs):
        """
        Saves data to the destination specified in args or kwargs.
        """
