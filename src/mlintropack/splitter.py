from abc import ABC, abstractmethod
from sklearn.model_selection import train_test_split

import torch.utils.data


class Splitter(ABC):

    @abstractmethod
    def __call__(self, *args, **kwargs):
        pass


class TorchSplitter(Splitter):

    def __call__(self, dataset, share, batch_size=1, shuffle=True):
        data_size = len(dataset)
        # Random split dataset
        train, test = torch.utils.data.random_split(dataset, [int(share * data_size), int((1 - share) * data_size)])
        train_dataloader = torch.utils.data.DataLoader(train, batch_size=batch_size, shuffle=shuffle)
        test_dataloader = torch.utils.data.DataLoader(test, batch_size=batch_size, shuffle=shuffle)
        return train_dataloader, test_dataloader


class NumpyScipySupervisedSplitter(Splitter):

    def __call__(self, dataset: dict, train_size=0.8, shuffle=True):
        train_x, test_x, train_y, test_y = train_test_split(
            dataset["features"],
            dataset["labels"],
            train_size=train_size,
            shuffle=True,
        )
        train_dataset = {
            "features": train_x,
            "labels": train_y
        }
        test_dataset = {
            "features": test_x,
            "labels": test_y
        }
        return train_dataset, test_dataset

