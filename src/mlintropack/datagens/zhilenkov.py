import numpy as np
import copy

from mlintropack.dataflows.dataflow import DataFlow
from mlintropack.datagens.datagen import DataGen


class LinearDataGenerator(DataGen):
    def __init__(self, num_features: int, num_obs: int, seed: int = 345):
        self.num_features = num_features
        self.num_obs = num_obs
        self.seed = seed
        self.x = None
        self.y = None

    def get_data(self, scale_x=10, scale_alpha=1, scale_beta=10, shift=5, noise_level=1., keep_in_object=True):
        np.random.seed(self.seed)
        alpha = np.random.randn(self.num_features, 1) * scale_alpha
        beta = np.random.randn() * scale_beta
        x = np.random.rand(self.num_obs, self.num_features) * scale_x
        x -= np.ones(shape=x.shape) * shift
        y = x @ alpha + beta * np.ones(shape=(self.num_obs, 1))
        noise = np.random.multivariate_normal(
            mean=np.zeros(shape=self.num_obs),
            cov=np.eye(self.num_obs)*noise_level,
            size=1
        ).T
        y += noise
        if keep_in_object:
            self.x = copy.deepcopy(x)
            self.y = copy.deepcopy(y)
        dataset = {
            "features": x,
            "labels": y
        }
        return dataset

    def generate_tricky_data_example(self, lift=8):
        def linear_one_dimensional_model_with_noise(  # Simple line f(x) = a * x + b = x by default values...
                arg: float,
                slope: float = 1.,
                bias: float = 0.,
                noise_level: float = 1.):
            return slope * arg + bias + np.random.randn() * noise_level
        num_obs = self.num_obs
        trick_feature_values = []
        x = np.linspace(-5, 5, num_obs)
        y = []
        np.random.seed(self.seed)
        for i in range(num_obs):
            trick_feature = 0. if np.random.rand() < 0.5 else lift
            if trick_feature:
                y.append(linear_one_dimensional_model_with_noise(x[i], bias=trick_feature))
            else:
                y.append(linear_one_dimensional_model_with_noise(x[i]))
            trick_feature_values.append(trick_feature)

        y = np.array(y)
        t = np.array(trick_feature_values)

        return x, y, t
