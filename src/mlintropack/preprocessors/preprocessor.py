from abc import ABC, abstractmethod


class Preprocessor(ABC):

    @abstractmethod
    def __call__(self, *args, **kwargs):
        pass


class DoNothingPreprocessor(Preprocessor):
    def __call__(self, dataset):
        return dataset

