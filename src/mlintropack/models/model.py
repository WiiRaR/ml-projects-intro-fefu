from abc import ABC, abstractmethod


class Model(ABC):

    @abstractmethod
    def fit(self, dataset):
        pass

    @abstractmethod
    def predict(self, dataset):
        pass
