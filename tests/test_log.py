from mlintropack.log import DEFAULT_LOGGING_CONFIG, configure_logger
import logging


def test_log(caplog):
    logger = configure_logger(logger_name="package", logger_config=DEFAULT_LOGGING_CONFIG)
    assert isinstance(logger, logging.Logger)
